#pragma once

namespace Version
{
	inline constexpr std::size_t MAJOR = 0;
	inline constexpr std::size_t MINOR = 8;
	inline constexpr std::size_t PATCH = 1;
	inline constexpr auto NAME = "0.8.1"sv;
	inline constexpr auto PROJECT = "BetterThirdPersonSelection"sv;
}
