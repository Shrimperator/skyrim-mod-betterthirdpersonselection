$BetterThidPersonSelection	Better Third Person Selection



$BetterThirdPersonSelection_GeneralPage	Allgemein
$BetterThirdPersonSelection_GeneralSettings_HeaderText	Allgemeine Einstellungen

$BetterThirdPersonSelection_IsEnabledInFirstPerson_OptionText	Aktiviert in Ego-Perspektive
$BetterThirdPersonSelection_IsEnabledInFirstPerson_InfoText	neuen Algorithmus zur Objekt-Selektion in der Ego-Perspektive aktivieren/deaktivieren. Hat keine Auswirkungen darauf, wie das ausgewählte Objekt angezeigt wird.

$BetterThirdPersonSelection_IsEnabledInThirdPerson_OptionText	Aktiviert in Third-Person Perspektive
$BetterThirdPersonSelection_IsEnabledInThirdPerson_InfoText	neuen Algorithmus zur Objekt-Selektion in der Third-Person Perspektive aktivieren/deaktivieren. Hat keine Auswirkungen darauf, wie das ausgewählte Objekt angezeigt wird.

$BetterThirdPersonSelection_IsEnabledOnHorseBack_OptionText	Aktiviert beim Reiten
$BetterThirdPersonSelection_IsEnabledOnHorseBack_InfoText	neuen Algorithmus zur Objekt-Selektion während des Reitens aktivieren/deaktivieren. Hat keine Auswirkungen darauf, wie das ausgewählte Objekt angezeigt wird.

$BetterThirdPersonSelection_IsEnabledInCombat_OptionText	Aktiviert im Kampf
$BetterThirdPersonSelection_IsEnabledInCombat_InfoText	neuen Algorithmus zur Objekt-Selektion während des Kampfes aktivieren/deaktivieren. Hat keine Auswirkungen darauf, wie das ausgewählte Objekt angezeigt wird.

$BetterThirdPersonSelection_IsEnabledWithWeaponsDrawn_OptionText	Aktiviert mit gezogener Waffe
$BetterThirdPersonSelection_IsEnabledWithWeaponsDrawn_InfoText	neuen Algorithmus zur Objekt-Selektion mit gezogener Waffe aktivieren/deaktivieren. Hat keine Auswirkungen darauf, wie das ausgewählte Objekt angezeigt wird.



$BetterThirdPersonSelection_ControlSettings_HeaderText	Steuerung

$BetterThirdPersonSelection_EnableObjectCycle_OptionText	Fokuswechsel aktivieren
$BetterThirdPersonSelection_EnableObjectCycle_InfoText	aktiviert/deaktiviert das wechseln zwischen Objekten in der Nähe durch Drücken von (Standard) Shift + Mausrad.

$BetterThirdPersonSelection_EnableContinuousObjectCycle_OptionText	Durchgehenden Fokuswechsel aktivieren
$BetterThirdPersonSelection_EnableContinuousObjectCycle_InfoText	wenn diese Option aktiviert ist, kann man immer weiter in die gleiche Richtung wechseln. Erreicht man das Ende, springt der Fokus wieder auf das erste Objekt.

$BetterThirdPersonSelection_ObjectCycleModifierKey_OptionText	Fokuswechsel Taste - Modifikator
$BetterThirdPersonSelection_ObjectCycleModifierKey_InfoText	wenn aktiviert, nutze diese Taste + [Fokuswechsel rauf / runter] um zwischen allen Objekten in der Umgebung zu wechseln.

$BetterThirdPersonSelection_ObjectCycleUpKey_OptionText	Fokuswechsel Taste - Rauf
$BetterThirdPersonSelection_ObjectCycleUpKey_InfoText	wenn aktiviert, nutze [Fokuswechsel Taste] + um zwischen allen Objekten in der Umgebung zu wechseln.

$BetterThirdPersonSelection_ObjectCycleDownKey_OptionText	Fokuswechsel Taste - Runter
$BetterThirdPersonSelection_ObjectCycleDownKey_InfoText	wenn aktiviert, nutze [Fokuswechsel Taste] + um zwischen allen Objekten in der Umgebung zu wechseln.

$BetterThirdPersonSelection_CycleDelayMS_OptionText	Fokuswechsel - Tastenverzögerung
$BetterThirdPersonSelection_CycleDelayMS_InfoText	Verzögerung in Millisekunden zwischen registrierten Tastenanschlägen beim Fokuswechsel.



$BetterThirdPersonSelection_SelectionRanges_HeaderText	Reichweiten-Einstellungen

$BetterThirdPersonSelection_MaxAngleDif_OptionText	Max Winkel
$BetterThirdPersonSelection_MaxAngleDif_InfoText	wie präzise die Kamera zum Objekt zeigen muss (2.0 = 360 Grad um den Spieler werden akzeptiert)

$BetterThirdPersonSelection_MaxAngleDif_HorseBack_OptionText	Max Winkel (beritten)
$BetterThirdPersonSelection_MaxAngleDif_HorseBack_InfoText	wie präzise die Kamera zum Objekt zeigen muss (2.0 = 360 Grad um den Spieler werden akzeptiert) - beritten

$BetterThirdPersonSelection_MaxInteractionRange_OptionText	Interaktions-Reichweite
$BetterThirdPersonSelection_MaxInteractionRange_InfoText	maximale Reichweite - in Skyrim Einheiten - zwischen dem Charakter und dem Objekt. Die native Fadenkreuz-Selektion ist davon ausgeschlossen und nutzt weiterhin die Standard-Einstellungen.

$BetterThirdPersonSelection_MaxInteractionRange_HorseBack_OptionText	Interaktions-Reichweite (beritten)
$BetterThirdPersonSelection_MaxInteractionRange_HorseBack_InfoText	maximale Reichweite - in Skyrim Einheiten - zwischen dem Charakter und dem Objekt - beritten. Die native Fadenkreuz-Selektion ist davon ausgeschlossen und nutzt weiterhin die Standard-Einstellungen.



$BetterThirdPersonSelection_HoldToDismount_HeaderText	Gedrückthalten zum Absteigen

$BetterThirdPersonSelection_IsHoldToDismountEnabled_HorseBack_OptionText	Gedrückthalten zum Absteigen aktivieren
$BetterThirdPersonSelection_IsHoldToDismountEnabled_HorseBack_InfoText	wenn diese Option aktiviert ist und der Charakter beritten ist, muss die Aktivierungs-Taste gedrückt gehalten werden zum absteigen.

$BetterThirdPersonSelection_HoldToDismountTime_HorseBack_OptionText	Tastendauer
$BetterThirdPersonSelection_HoldToDismountTime_HorseBack_InfoText	wenn Gedrückthalten zum Absteigen aktiviert ist, gibt diese Option die Dauer in Sekunden an, für die die Taste insgesamt gedrücktgehalten werden muss.

$BetterThirdPersonSelection_HoldToDismountMinTime_HorseBack_OptionText	Tastendauer
$BetterThirdPersonSelection_HoldToDismountMinTime_HorseBack_InfoText	wenn Gedrückthalten zum Absteigen aktiviert ist, gibt diese Option die Dauer in Sekunden an, für die die Taste gedrücktgehalten werden muss ehe der Progress Circle angezeigt wird.



$BetterThirdPersonSelection_WidgetPage	Widgets
$BetterThirdPersonSelection_WidgetSettings_HeaderText	Widget Einstellungen

$BetterThirdPersonSelection_IsWidget3DEnabledInFirstPerson_OptionText	3D Widget aktiviert - Ego-Perspektive
$BetterThirdPersonSelection_IsWidget3DEnabledInFirstPerson_InfoText	3D widget aktiviert/deaktiviert - Ego-Perspektive

$BetterThirdPersonSelection_IsWidget3DEnabledInThirdPerson_OptionText	3D Widget aktiviert - Third-Person Perspektive
$BetterThirdPersonSelection_IsWidget3DEnabledInThirdPerson_InfoText	3D widget aktiviert/deaktiviert - Third-Person Perspektive

$BetterThirdPersonSelection_IsWidget3DEnabledOnHorseBack_OptionText	3D Widget aktiviert - beritten
$BetterThirdPersonSelection_IsWidget3DEnabledOnHorseBack_InfoText	e3D widget aktiviert/deaktiviert - beritten

$BetterThirdPersonSelection_IsWidget3DEnabledInCombat_OptionText	3D Widget aktiviert - im Kampf
$BetterThirdPersonSelection_IsWidget3DEnabledInCombat_InfoText	3D widget aktiviert/deaktiviert - im Kampf

$BetterThirdPersonSelection_IsWidget3DEnabledWithWeaponsDrawn_OptionText	3D Widget aktiviert - Waffe gezogen
$BetterThirdPersonSelection_IsWidget3DEnabledWithWeaponsDrawn_InfoText	3D widget aktiviert/deaktiviert - Waffe gezogen



$BetterThirdPersonSelection_WidgetStyleSettings_HeaderText	3D Widget Style

$BetterThirdPersonSelection_IsIsDividerEnabled_OptionText	Graue Trennlinie aktiviert
$BetterThirdPersonSelection_IsDividerEnabled_InfoText	graue Trennlinie zwischen Objektname und Info (Wert, Gewicht) aktiviert/deaktiviert

$BetterThirdPersonSelection_IsActivationButtonEnabled_OptionText	Aktivierungs-Button aktiviert
$BetterThirdPersonSelection_IsActivationButtonEnabled_InfoText	Aktivierungs-Button aktiviert/deaktiviert

$BetterThirdPersonSelection_IsItemInfoEnabled_OptionText	Object Info aktiviert
$BetterThirdPersonSelection_IsItemInfoEnabled_InfoText	Item Info (Wert, Gewicht) aktiviert/deaktiviert

$BetterThirdPersonSelection_IsDismountProgressCirlcleEnabled_OptionText	Fortschritts-Kreis aktiviert
$BetterThirdPersonSelection_IsDismountProgressCirlcleEnabled_InfoText	beim Absteigen mit Gedrückthalten zum Absteigen wird ein Fortschritts-Kreis angezeigt

$BetterThirdPersonSelection_WidgetAlpha_OptionText	Widget Sichtbarkeit
$BetterThirdPersonSelection_WidgetAlpha_InfoText	Alpha-Wert von allen GUI Elementen (0.0-100.0). Verändert auch das 2D-Widget

$BetterThirdPersonSelection_WidgetZOffset_OptionText	3D Widget Höhe
$BetterThirdPersonSelection_WidgetZOffset_InfoText	bewegt das 3D Widget bei einigen Objekten weiter nach oben (oder nach unten für negative Werte). Hat keinen Effekt auf Objekte deren 3D Widget nicht über dem Objekt platziert ist, oder wenn die Position des Widgets per Konfig Datei angepasst wurde

$BetterThirdPersonSelection_WidgetZOffsetFirstPerson_OptionText	3D Widget Höhe (Ego-Perspektive)
$BetterThirdPersonSelection_WidgetZOffsetFirstPerson_InfoText	bewegt das 3D Widget bei einigen Objekten weiter nach oben (oder nach unten für negative Werte). Diese Option ist nur in der Ego-Perspektive aktiviert



$BetterThirdPersonSelection_2DWidgetStyleSettings_HeaderText	2D Widget Style

$BetterThirdPersonSelection_Widget2DSize_OptionText	2D Widget Größe
$BetterThirdPersonSelection_Widget2DSize_InfoText	Größe des 2D Widgets. Damit ist das Widget gemeint, das aktiviert ist während das 3D-Widget deaktiviert ist.

$BetterThirdPersonSelection_Widget2DXPos_OptionText	2D Widget Pos X
$BetterThirdPersonSelection_Widget2DXPos_InfoText	X Position des 2D Widgets, als Prozentsatz der Bildschirmbreite (0.0 - 1.0)

$BetterThirdPersonSelection_Widget2DYPos_OptionText	2D Widget Pos Y
$BetterThirdPersonSelection_Widget2DYPos_InfoText	Y Position des 2D Widgets, als Prozentsatz der Bildschirmhöhe (0.0 - 1.0)



$BetterThirdPersonSelection_fWidgetFadeInDelta_OptionText	Widget Fade In Geschwindigkeit
$BetterThirdPersonSelection_fWidgetFadeInDelta_InfoText	Wie schnell das Widget eingeblendet werden soll. Auf 0.0 oder 1.0 setzen um sofort einzublenden.

$BetterThirdPersonSelection_fWidgetFadeOutDelta_OptionText	Widget Fade Out Geschwindigkeit
$BetterThirdPersonSelection_fWidgetFadeOutDelta_InfoText	Wie schnell das Widget ausgeblendet werden soll. Auf 0.0 oder 1.0 setzen um sofort auszublenden.



$BetterThirdPersonSelection_WidgetSizeMin_OptionText	Min Widget Größe
$BetterThirdPersonSelection_WidgetSizeMin_InfoText	Widget Größe wenn das selektierte Objekt am weitesten entfernt ist.

$BetterThirdPersonSelection_WidgetSizeMax_OptionText	Max Widget Größe
$BetterThirdPersonSelection_WidgetSizeMax_InfoText	Widget Größe wenn das selektierte Objekt am wenigsten weit entfernt ist.

$BetterThirdPersonSelection_WidgetSizeMin_HorseBack_OptionText	Min Widget Größe (beritten)
$BetterThirdPersonSelection_WidgetSizeMin_HorseBack_InfoText	Widget Größe wenn das selektierte Objekt am weitesten entfernt ist - beritten.

$BetterThirdPersonSelection_WidgetSizeMax_HorseBack_OptionText	Max Widget Größe (beritten)
$BetterThirdPersonSelection_WidgetSizeMax_HorseBack_InfoText	Widget Größe wenn das selektierte Objekt am wenigsten weit entfernt ist - beritten.



$BetterThirdPersonSelection_PriorityPage	Priorität
$BetterThirdPersonSelection_PriorityPage_HeaderText	Priority Settings

$BetterThirdPersonSelection_AngleMult_OptionText	Winkel Priorität Multiplikator
$BetterThirdPersonSelection_AngleMult_InfoText	höhere Werte bedeuten, dass die Differenz zwischen dem Winkel von der Kamera geradeaus und dem Winkel von der Kamera zum Zielobjekt einen größeren Einfluss darauf haben, ob das jeweilige Objekt selektiert wird.

$BetterThirdPersonSelection_DistMult_OptionText	Abstand Priorität Multiplikator
$BetterThirdPersonSelection_DistMult_InfoText	höhere Werte bedeuten, dass die Distanz zwischen dem Spieler und dem Zielobjekt einen größeren Einfluss darauf haben, ob das jeweilige Objekt selektiert wird.

$BetterThirdPersonSelection_TypeMult_OptionText	Typ Priorität Multiplikator
$BetterThirdPersonSelection_TypeMult_InfoText	höhere Werte bedeuten, dass der Objekttyp (z.B. Waffe, Rüstung, Gegenstand) einen größeren Einfluss darauf haben, ob das jeweilige Objekt selektiert wird.

$BetterThirdPersonSelection_SpecificMult_OptionText	Specific Objects Priorität Multiplikator
$BetterThirdPersonSelection_SpecificMult_InfoText	höhere Werte bedeuten, dass Objekte aus einer hardgecodeten Liste eher selektiert werden als andere Objekte (bisher nur Goldmünzen).

$BetterThirdPersonSelection_ValueMult_OptionText	Gold Value Priorität Multiplikator
$BetterThirdPersonSelection_ValueMult_InfoText	höhere Werte bedeuten, dass der Wert eines Gegenstands einen größeren Einfluss darauf hat, ob das jeweilige Objekt selektiert wird.

$BetterThirdPersonSelection_MaxValue_OptionText	Max Gold Value
$BetterThirdPersonSelection_MaxValue_InfoText	Max-Wert für die interne Berechnung des Goldwerts. Sehr niedrige Werte bedeuten, dass kleinere Unterschiede im Wert von Gegenständen einen größeren Einfluss auf die Priorität haben.

$BetterThirdPersonSelection_NativeSelectionBonus_OptionText	Bonus für native Selektion
$BetterThirdPersonSelection_NativeSelectionBonus_InfoText	Bonus für das Objekt das bei der nativen Fadenkreuz-Selektion ausgewählt wurde. Ein sehr hoher Wert bedeutet, dass das native Objekt immer ausgewählt wird, wenn eines gefunden wurde.

$BetterThirdPersonSelection_PreviousSelectionBonus_OptionText	Bonus für vorherige Selektion
$BetterThirdPersonSelection_PreviousSelectionBonus_InfoText	Bonus für das vorherige ausgewählte Objekt. Sollte ein kleiner Wert > 0 sein. Das sorgt dafür, dass der Fokus weniger "flackert"

$BetterThirdPersonSelection_NPCSelectionBonus_OptionText	Bonus für NPCs
$BetterThirdPersonSelection_NPCSelectionBonus_InfoText	Höhere Werte machen es einfacher, NPCs zu selektieren. Andere Objekte direkt neben NPCs zu selektieren wird allerdings schwieriger.


$BetterThirdPersonSelection_PerformancePage	Performance
$BetterThirdPersonSelection_PerformancePage_HeaderText	Leistung

$BetterThirdPersonSelection_NumTracesHorizontal_OptionText	Traces Horizontal
$BetterThirdPersonSelection_NumTracesHorizontal_InfoText	Maximale Anzahl von horizontalen Ray Traces die pro Objekt gezeichnet werden. Die gesamtzahl von Traces pro Objekt sind Traces Horizontal * Traces Vertikal. Mehr traces = präzisere Okklusions-Erkennung (ist Objekt verdeckt?), aber größere Auswirkung auf Performance.

$BetterThirdPersonSelection_NumTracesVertical_OptionText	Traces Vertical
$BetterThirdPersonSelection_NumTracesVertical_InfoText	Maximale Anzahl von vertikalen Ray Traces die pro Objekt gezeichnet werden. Die gesamtzahl von Traces pro Objekt sind Traces Horizontal * Traces Vertikal. Mehr traces = präzisere Okklusions-Erkennung (ist Objekt verdeckt?), aber größere Auswirkung auf Performance.



$BetterThirdPersonSelection_DebugPage	Debug
$BetterThirdPersonSelection_DebugPage_HeaderText	Debug Einstellungen

$BetterThirdPersonSelection_Draw_RayTraces_OptionText	Debug Ray Traces
$BetterThirdPersonSelection_Draw_RayTraces_InfoText	aktiviert/deaktiviert Scaleform debugging Ray Traces. Funktioniert nur während neue Objekt-Selektion aktiviert ist.

$BetterThirdPersonSelection_Draw_Bounds_Box_OptionText	Debug Bounds - Box
$BetterThirdPersonSelection_Draw_Bounds_Box_InfoText	aktiviert/deaktiviert Scaleform debugging Boxen. Funktioniert nur während neue Objekt-Selektion aktiviert ist.

$BetterThirdPersonSelection_Draw_Bounds_Sphere_OptionText	Debug Bounds - Sphere
$BetterThirdPersonSelection_Draw_Bounds_Sphere_InfoText	aktiviert/deaktiviert Scaleform debugging Sphären. Funktioniert nur während neue Objekt-Selektion aktiviert ist.

$BetterThirdPersonSelection_Draw_Bounds_MinMax_OptionText	Debug Bounds - Box min/max
$BetterThirdPersonSelection_Draw_Bounds_MinMax_InfoText	aktiviert/deaktiviert Scaleform debugging Box - min/max. Funktioniert nur während neue Objekt-Selektion aktiviert ist.

$BetterThirdPersonSelection_DebugDrawThickness_OptionText	Line Thickness
$BetterThirdPersonSelection_DebugDrawThickness_InfoText	Linien Durchmesser in Pixeln für alle Debug Linien.

$BetterThirdPersonSelection_DebugDrawAlpha_OptionText	Line Sichtbarkeit
$BetterThirdPersonSelection_DebugDrawAlpha_InfoText	Alpha-Wert für alle Debug-Linien.



$BetterThirdPersonSelection_AdditionalSettings_HeaderText	Zusätzliche Einstellungen

$BetterThirdPersonSelection_HotReloadConfig_OptionText	Hot Reload Filters
$BetterThirdPersonSelection_HotReloadConfig_InfoText	aktiviert/deaktiviert das erneute Einlesen der ObjectFilterList.toml jedes mal wenn das BTPS MCM geschlossen wird. Nützlich während der Konfiguration der Filter.

$BetterThirdPersonSelection_ShowFurnitureMarkers_OptionText	Möbel Markierungen anzeigen
$BetterThirdPersonSelection_ShowFurnitureMarkers_InfoText	aktiviert/deaktiviert diverse Markierungen, die sonst nur NPCs verwenden können.



$BetterThirdPersonSelection_FilterPage	Filter
$BetterThirdPersonSelection_FilterSettings_HeaderText	Filter Einstellungen

$BetterThirdPersonSelection_Filter_InfoText	aktiviere/deaktiviere Filter.

$BetterThirdPersonSelection_EnableFiltersForNativeSelection_OptionText	aktiviere Filter während Fadenkreuz-Selektion
$BetterThirdPersonSelection_EnableFiltersForNativeSelection_InfoText	aktiviert/deaktiviert of die Filter auch dann benutzt werden sollen, wenn die eigentliche BTPS Selektion nicht aktiv ist. Z.B. standardmäßig in der Ego-Perspektive



$BetterThirdPersonSelection_CompatibilityPage	Kompatibilität
$BetterThirdPersonSelection_CompatibilitySettings_HeaderText	Kompatibilitäts-Einstellungen

$BetterThirdPersonSelection_AdjustMoreHudWidgets_OptionText	aktiviert/deaktiviert MoreHUD Kompatibilität
$BetterThirdPersonSelection_AdjustMoreHudWidgets_InfoText	wenn diese Option aktiv ist, werden diverse MoreHUD-Elemente dynamisch zum Interaktionstext-Widget bewegt. Bei einigen Seitenverhältnissen kann dies zu Problemen führen. Braucht eventuell einen Neustart um vollständig aktiv zu werden