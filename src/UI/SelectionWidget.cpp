#pragma once
#include "SelectionWidget.h"
#include "RevE/Offsets.h"
#include "RevE/Hooks.h"
#include "FocusManager.h"
#include "API/Compatibility.h"
#include "Settings.h"
#include "UI/DebugAPI.h"

#include "windows.h"

std::optional<glm::dvec3> SelectionWidget::LastWidgetPos;

glm::vec2 SelectionWidget::WidgetPos;
glm::vec2 SelectionWidget::WidgetSize;

//

bool SelectionWidget::OriginalValuesSet = false;

double SelectionWidget::FadeProgress = 0.0;
EUIFadeMode SelectionWidget::FadeMode = NONE;

std::string SelectionWidget::LastActivateText;
std::string SelectionWidget::LastActivateName;

bool SelectionWidget::SelectionIs2DOverride = false;
bool SelectionWidget::UpdateFocusObjDeferred = false;

std::shared_ptr<FocusObject> SelectionWidget::UIFocusRef = nullptr;

glm::vec2 SelectionWidget::OrigMCPos;
glm::vec2 SelectionWidget::OrigMCSize;

double SelectionWidget::Widget2DSize = 1.0;
glm::vec2 SelectionWidget::Widget2DPos = glm::vec2(0.5, 0.35);

float SelectionWidget::ProgressCircleValue = 0.0f;

std::vector<std::string> SelectionWidgetMenu::Hidden_Sources;
bool SelectionWidgetMenu::IsEnabled = false;
bool SelectionWidgetMenu::IsVisible = false;

std::mutex m_setEnabled;
std::recursive_mutex m_update;
std::mutex m_hideHUD;
std::mutex m_selectionChanged;

std::recursive_mutex m_show;
std::recursive_mutex m_hide;
std::mutex m_toggleVisibility;

std::mutex m_load;
std::mutex m_unload;
std::mutex m_updateText;
std::recursive_mutex m_manipulateMoreHUD;
std::mutex m_setFocusRef;
std::mutex m_clearSelectionText;

std::mutex m_getWidgetPos;
std::mutex m_setWidgetPos;
std::mutex m_getWidgetSize;
std::mutex m_setWidgetSize;

void SelectionWidget::OnChangeActivateText()
{
	auto currentFocusObj = FocusManager::GetActiveFocusObject();
	if (!currentFocusObj || !currentFocusObj->ObjectRef)
	{
		OnUpdateCrosshairText2D();
	}
	else
	{
		SelectionIs2DOverride = false;
	}
}

void SelectionWidget::OnSelectionChanged()
{
	std::lock_guard lock(m_selectionChanged);

	if (Settings::GetWidget3DEnabled())
	{
		auto focusObj = FocusManager::GetActiveFocusObject();
		if (focusObj && focusObj->ObjectRef)
		{
			if (Settings::WidgetFadeInDelta > 0.0 && Settings::WidgetFadeInDelta < 1.0)
				StartFadeIn();
			else
				FadeInInstant();
		}
		else
		{
			if (Settings::WidgetFadeOutDelta > 0.0 && Settings::WidgetFadeOutDelta < 1.0)
				StartFadeOut();
			else
				FadeOutInstant();
		}

		if (Compatibility::MoreHUD::IsEnabled)
		SetMoreHUDWidgetVisibility(false);
	}

	Update();
}

void SelectionWidget::TryUpdateSelectionBounds()
{
	auto newFocusObj = FocusManager::GetActiveFocusObject();
	auto oldFocusObj = GetUIFocusRef();

	// in case the focused ref is still the same, update the FocusObject 
	// (which contains the reference and object bounds, so basically: update UIFocusRef with newly calculated bounds and position)
	if (newFocusObj && oldFocusObj && newFocusObj->ObjectRef == oldFocusObj->ObjectRef)
	{
		SetUIFocusRef(newFocusObj);
	}
}

void SelectionWidget::SetUIFocusRef(const std::shared_ptr<FocusObject>& focusRef)
{
	std::lock_guard lock(m_setFocusRef);
	UIFocusRef = focusRef;
}

std::shared_ptr<FocusObject> SelectionWidget::GetUIFocusRef()
{
	std::lock_guard lock(m_setFocusRef);
	return UIFocusRef;
}

void SelectionWidgetMenu::SetEnabled(bool mode)
{
	std::lock_guard lock(m_setEnabled);

	if (IsEnabled != mode)
	{
		IsEnabled = mode;
		ToggleVisibility(true);
	}
}

glm::vec2 SelectionWidget::GetWidgetPos()
{
	std::lock_guard lock(m_getWidgetPos);
	return WidgetPos;
}

void SelectionWidget::SetWidgetPos(glm::vec2 pos)
{
	std::lock_guard lock(m_setWidgetPos);
	WidgetPos = pos;
}

glm::vec2 SelectionWidget::GetWidgetSize()
{
	std::lock_guard lock(m_getWidgetSize);
	return WidgetSize;
}

void SelectionWidget::SetWidgetSize(glm::vec2 size)
{
	std::lock_guard lock(m_setWidgetSize);
	WidgetSize = size;
}

void SelectionWidget::OnPerspectiveToggled(bool cond)
{
}

void SelectionWidget::OnNativeSelectionChanged(RE::TESObjectREFR* nativeSelection)
{
}

void SelectionWidget::OnUpdateCrosshairText()
{
	// for an explanation of this spaghetti solution of horrors, see declaration of UpdateFocusObjDeferred
	if (UpdateFocusObjDeferred)
	{
		SKSE::GetTaskInterface()->AddUITask([]()
		{
			SetUIFocusRef(FocusManager::GetActiveFocusObject());
		});
		UpdateFocusObjDeferred = false;
	}
}

void SelectionWidget::OnUpdateCrosshairText2D()
{
	// things like AutoLoadDoors: they change the crosshair text, but don't set the crosshair ref
	// make sure the widget is faded in for those. This has to be here instead of at 3D check, because
	// after fading out, 3D check fails for a little moment, but widget should be faded out
	FadeInInstant();
	SelectionIs2DOverride = true;
}

double SelectionWidget::GetMaxCameraDist()
{
	auto playerChar = RE::PlayerCharacter::GetSingleton();

	if (!playerChar || !playerChar->IsOnMount())
		return MAX_CAMERA_DIST;
	return MAX_CAMERA_DIST_HORSEBACK;
}

void SelectionWidget::Update()
{
	std::lock_guard lock(m_update);

	SelectionWidget::HideNativeHUD();

	TryUpdateSelectionBounds();

	ProgressFade();
	UpdateWidgetText();
	UpdateTDMVisibility();
	UpdateWidgetPos();

	if (Compatibility::MoreHUD::IsEnabled)
		SelectionWidget::UpdateMoreHUDWidget();
}

void SelectionWidget::UpdateWidgetPos()
{
	auto playerChar = RE::PlayerCharacter::GetSingleton();
	if (!playerChar)
		return;

	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie)
		return;

	InitHUDValues();
	if (!OriginalValuesSet)
		return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");
	if (!selectionWidget.IsObject())
		return;

	UpdateProgressCircle(widgetMenu);

	auto focusObj = GetUIFocusRef();

	glm::dvec3 widgetPos;

	if (!Settings::GetWidget3DEnabled() ||
		!Get3DWidgetPos(focusObj.get(), widgetPos)) // Get3DWidgetPos fails on things like AutoLoadDoors for example - draw those 2D
	{
		SetWidgetPos(Get2DWidgetPos(widgetMenu));
		SetWidgetSize(Get2DWidgetSize());

		SetElementPos(GetWidgetPos(), selectionWidget);
		SetElementSize(GetWidgetSize(), selectionWidget);

		if (FadeMode != FADED)
			SetSelectionAlpha(Settings::WidgetAlpha, selectionWidget);
	}
	else
	{
		SetWidgetPos(GetScreenLoc(widgetMenu, widgetPos));
		SetWidgetPos(ClampWidgetToScreenRect(widgetMenu, GetWidgetPos(), selectionWidget));

		SetWidgetSize(Get3DWidgetSize(focusObj.get()));

 		SetElementSize(GetWidgetSize(), selectionWidget);
 		SetElementPos(GetWidgetPos(), selectionWidget);

		LastWidgetPos = widgetPos;
	}
}

constexpr float ACTIVATE_BUTTON_OFFSET_LEFT = 10.0;

void SelectionWidget::UpdateWidgetText()
{
	std::lock_guard lock(m_updateText);

	auto ui = RE::UI::GetSingleton();
	if (!ui) return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie) return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie) return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");
	if (!selectionWidget.IsObject()) return;

	RE::GFxValue rolloverText;
	RE::GFxValue rolloverInfoText;
	RE::GFxValue rolloverGrayBar_mc;	
	RE::GFxValue rolloverButton_tf;

	hud->uiMovie->GetVariable(&rolloverText, "HUDMovieBaseInstance.RolloverText");
	hud->uiMovie->GetVariable(&rolloverInfoText, "HUDMovieBaseInstance.RolloverInfoText");
	hud->uiMovie->GetVariable(&rolloverButton_tf, "HUDMovieBaseInstance.RolloverButton_tf");
	hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverGrayBar_mc");

	// ClearUI compatibility - probably messes with the actionscript somewhere and removes 'RolloverGrayBar_mc'
	if (!rolloverGrayBar_mc.IsObject())
		hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverInfo_mc.RolloverInfoInstance");

	if (!rolloverText.IsObject() || !rolloverInfoText.IsObject() || !rolloverGrayBar_mc.IsObject() || !rolloverButton_tf.IsObject())
		return;

	auto nativeHtmlString = GetGFxMember(rolloverText, "htmlText");
	auto stringName = GetGFxMember(rolloverText, "text");
    auto htmlStringInfo = GetGFxMember(rolloverInfoText, "htmlText");
	auto stringInfo = GetGFxMember(rolloverInfoText, "text");
	auto htmlStringButton = GetGFxMember(rolloverButton_tf, "htmlText");

	auto activateText = GetGFxMember(selectionWidget, "ActivateText");
	auto infoText = GetGFxMember(selectionWidget, "InfoText");
	auto grayBar = GetGFxMember(selectionWidget, "GrayBar");

	if (!activateText.IsObject() || 
		!infoText.IsObject() || 
		!grayBar.IsObject())
		return;

	std::string sanitizedHTMLString = 
		Compatibility::MoreHUD::IsEnabled ? 
		Compatibility::MoreHUD::StripHTMLIconPlaceholders(nativeHtmlString.ToString().c_str()) : 
		nativeHtmlString.ToString().c_str();

	auto activateButton = GetGFxMember(selectionWidget, "ActivateButtonInstance");
	if (!activateButton.IsObject())
		return;

	auto activateButtonText = GetGFxMember(activateButton, "Text");
	if (!activateButtonText.IsObject())
		return;

	auto focusObj = GetUIFocusRef();

	SetSelectionAlpha(GetWidgetCurrentAlpha(), selectionWidget);
	
	if (FadeMode != FADE_OUT)
	{
		std::string currActivateText = std::string(nativeHtmlString.ToString());
		std::string currActivateName = std::string(stringName.ToString());

		if (currActivateName != LastActivateName)
		{
			activateText.SetTextHTML(sanitizedHTMLString.c_str());
			infoText.SetTextHTML(htmlStringInfo.GetString());
			activateButtonText.SetTextHTML(htmlStringButton.GetString());

			LastActivateText = currActivateText;
			LastActivateName = currActivateName;
			OnChangeActivateText();

			bool grayBarVisible = Settings::IsDividerEnabled && !Util::IsEmpty(stringInfo.GetString());

			// show if infotext isn't empty
			grayBar.SetMember("_visible", grayBarVisible);
			infoText.SetMember("_visible", Settings::IsItemInfoEnabled);
		}
	}

	// disable activate button for containers if quickloot is installed
    bool quickLootActive = Compatibility::QuickLootRE::IsEnabled && focusObj && focusObj->ObjectRef.get() && Compatibility::QuickLootRE::CanOpen(focusObj->ObjectRef.get().get());

	//glm::dvec3 widgetPos;
	// if button is disabled globally, or no activation object, or autoloaddoor
	if (!Settings::IsActivationButtonEnabled ||
		!focusObj ||
		(focusObj && !focusObj->ObjectRef.get()) || 
		quickLootActive ||
		//(Util::IsEmpty(stringName.GetString()) && !grayBarVisible) || 
		Util::IsFirstLineEmpty(stringName.GetString()) || 
		SelectionIs2DOverride)
		//!Get3DWidgetPos(focusObj.get(), widgetPos))
	{
		activateButton.SetMember("_visible", false);
	}
	else
	{
		RE::GFxValue lineMetricsOut;
		RE::GFxValue args[1]{ 0 };

		try { activateText.Invoke("getLineMetrics", &lineMetricsOut, args, 1); }
		catch (std::exception& e) {}

 		auto lineMetricsX = GetGFxMember(lineMetricsOut, "width").GetNumber();
		lineMetricsX = PixelToStageCoordinates(widgetMenu, glm::vec2(lineMetricsX, 0.0)).x;

		activateButton.SetMember("_x", -lineMetricsX - ACTIVATE_BUTTON_OFFSET_LEFT);
		activateButton.SetMember("_visible", true);
	}
}

void SelectionWidget::HideNativeHUD()
{
	std::lock_guard lock(m_hideHUD);

	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie)
		return;

	RE::GFxValue rolloverText;
	RE::GFxValue rolloverInfoText;
	RE::GFxValue rolloverGrayBar_mc;
	RE::GFxValue rolloverButton_tf;

	hud->uiMovie->GetVariable(&rolloverText, "HUDMovieBaseInstance.RolloverText");
	hud->uiMovie->GetVariable(&rolloverInfoText, "HUDMovieBaseInstance.RolloverInfoText");
	hud->uiMovie->GetVariable(&rolloverButton_tf, "HUDMovieBaseInstance.RolloverButton_tf");
	hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverGrayBar_mc");

	// ClearUI compatibility - probably messes with the actionscript somewhere and removes 'RolloverGrayBar_mc'
	if (!rolloverGrayBar_mc.IsObject())
		hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverInfo_mc.RolloverInfoInstance");

	if (!rolloverText.IsObject() || !rolloverInfoText.IsObject() || !rolloverGrayBar_mc.IsObject() || !rolloverButton_tf.IsObject())
		return;

	rolloverText.SetMember("_visible", false);
	rolloverInfoText.SetMember("_visible", false);
	rolloverGrayBar_mc.SetMember("_visible", false);
	rolloverButton_tf.SetMember("_visible", false);
}

void SelectionWidget::UpdateProgressCircle(RE::GPtr<RE::IMenu> menu)
{
	if (!menu || !menu->uiMovie)
		return;

	RE::GFxValue progressCirlce;
	menu->uiMovie->GetVariable(&progressCirlce, "ProgressCircleInstance");

	if (!progressCirlce.IsObject())
		return;

	progressCirlce.SetMember("_alpha", Settings::WidgetAlpha);

	if (ProgressCircleValue == 0.0f)
		progressCirlce.SetMember("_visible", false);
	else
	{
		progressCirlce.SetMember("_visible", true);

		RE::GFxValue args[1]{ ProgressCircleValue };

		try
		{
			progressCirlce.Invoke("SetProgressCircleProgress", nullptr, args, 1);
		}
		catch (std::exception& e) {}
	}
}

void SelectionWidget::UpdateTDMVisibility()
{
	using tdmCompat = Compatibility::TrueDirectionalMovement;
    auto focusObj = GetUIFocusRef();

	if (tdmCompat::IsEnabled && tdmCompat::IsTrueHUDEnabled && Settings::GetWidget3DEnabled() &&
		focusObj && focusObj->ObjectRef.get())
	{
        auto actorRef = focusObj->ObjectRef.get()->As<RE::Actor>();

        if (tdmCompat::TrueHUDAPIInterface)
        {
            if (actorRef)
            {
                auto actorRefHandle = actorRef->GetHandle();
				if (tdmCompat::TrueHUDAPIInterface->HasInfoBar(actorRefHandle))
				{
                    SelectionWidgetMenu::Hide("TDM");
                    return;
				}
            }
        }
		// less good fallback solution for old versions of TrueHUD - registering the API
		// will have failed in that case, despite TrueHUD being installed
        else
		{
            if (actorRef && actorRef->IsInCombat() &&
                actorRef->AsActorState()->GetLifeState() == RE::ACTOR_LIFE_STATE::kAlive)
			{
				SelectionWidgetMenu::Hide("TDM");
				return;
			}
		}
	}

	SelectionWidgetMenu::Show("TDM");
}

glm::vec2 SelectionWidget::ToGlobal(glm::vec2 posIn, RE::GFxValue element, RE::GPtr<RE::IMenu> menu)
{
	if (!menu || !menu->uiMovie || !element.IsObject())
		return posIn;

	RE::GFxValue objPoint;
	menu->uiMovie->CreateObject(&objPoint);
	objPoint.SetMember("x", posIn.x);
	objPoint.SetMember("y", posIn.y);

	RE::GFxValue gfxResult;
	RE::GFxValue args[1]{ objPoint };
	try
	{
		element.Invoke("globalToLocal", &gfxResult, args, 1);
	}
	catch (std::exception& e) {}

	double newX = GetGFxMember(args[0], "x").GetNumber();
	double newY = GetGFxMember(args[0], "y").GetNumber();

	return {newX, newY};
}

double MoreHUDIconsOffsetRight = 60.0;
double MoreHUDIconMargin = 10.0;

double MoreHUDEffectsSizeFact = 1.25;
double InfoWidgetOffsetLeft = 75.0;

void SelectionWidget::UpdateMoreHUDWidget()
{
	std::lock_guard lock(m_manipulateMoreHUD);

	auto ui = RE::UI::GetSingleton();
	if (!ui) return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie) return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie) return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");

	if (!selectionWidget.IsObject()) return;
	auto activateText = GetGFxMember(selectionWidget, "ActivateText");

	auto grayBar = GetGFxMember(selectionWidget, "GrayBar");
	if (!grayBar.IsObject()) return;

	double sizeFactor = WidgetSize.x / 100.0;

	auto textWidth = GetGFxMember(activateText, "textWidth").GetNumber();
	textWidth = (PixelToStageCoordinates(hud, glm::vec2(textWidth, 0.0)).x * sizeFactor);

	auto textHeight = GetGFxMember(activateText, "textHeight").GetNumber();
	textHeight = PixelToStageCoordinates(hud, glm::vec2(0.0, textHeight)).y * sizeFactor;

	auto SelectionWidgetHalfHeight = GetGFxMember(selectionWidget, "_height").GetNumber() / 2.0;

	auto grayBarHalfWidth = (GetGFxMember(grayBar, "_width").GetNumber() / 2.0) * sizeFactor;

	//
	// position 'content', which is the MoreHUD widget that displays
	// magic effects on alchemical substances and food for example
	//

	RE::GFxValue widgetCont;
	hud->uiMovie->GetVariable(&widgetCont, "AHZWidgetContainer");
	if (!widgetCont.IsObject()) return;

	auto ahzWidget = GetGFxMember(widgetCont, "AHZWidget");
	if (!ahzWidget.IsObject()) return;

	auto content = GetGFxMember(ahzWidget, "content");
	if (!content.IsObject()) return;

	double effectsSize = WidgetSize.x * MoreHUDEffectsSizeFact;
	content.SetMember("_xscale", effectsSize);
	content.SetMember("_yscale", effectsSize);

	// position to the right of the selection text, but don't intersect gray separation bar
	auto maxHalfWidth = glm::max(textWidth, grayBarHalfWidth);

	auto widgetPos = GetWidgetPos();

	glm::vec2 containerPos(widgetPos.x + maxHalfWidth + (InfoWidgetOffsetLeft * sizeFactor), widgetPos.y - SelectionWidgetHalfHeight);
	auto scaledContainerPos = ToScaledAspectRatioCoordinates(hud, containerPos);

    content.SetMember("_x", scaledContainerPos.x);
    content.SetMember("_y", scaledContainerPos.y);

	//
	// position icons
	//

	RE::GFxValue iconCont;
	hud->uiMovie->GetVariable(&iconCont, "ahz.scripts.widgets.AHZHudInfoWidget.IconContainer");
	if (!iconCont.IsObject()) return;

	auto textField = GetGFxMember(iconCont, "_tf");
	if (!textField.IsObject()) return;
	
	auto hudRoot = GetGFxMember(textField, "_parent");
	if (!hudRoot.IsObject()) return;

	auto loadedIcons = GetGFxMember(iconCont, "loadedIcons");
	if (!loadedIcons.IsArray()) return;

	auto gfxIconSize = GetGFxMember(iconCont, "_iconSize");
	if (!gfxIconSize.IsNumber()) return;

	auto gfxIconScale = GetGFxMember(iconCont, "_iconScale");
	if (!gfxIconScale.IsNumber()) return;

	int arraySize = loadedIcons.GetArraySize();
	double iconSize = gfxIconSize.GetNumber() * gfxIconScale.GetNumber();
	double iconRealSize = iconSize * sizeFactor;

	auto scaledWidgetPos = ToScaledAspectRatioCoordinates(hud, glm::vec2(widgetPos.x - textWidth - MoreHUDIconsOffsetRight * sizeFactor, widgetPos.y));

	for (int i = 0; i < arraySize; i++)
	{
		RE::GFxValue currIcon;
		loadedIcons.GetElement(i, &currIcon);

		if (!currIcon.IsObject())
			continue;

		// size of the icon container. Could probably get dynamically, but I don't think it matters
        glm::vec2 iconPos(640, 480);

		iconPos = ToScaledAspectRatioCoordinates(hud, iconPos);

		currIcon.SetMember("_x", -iconPos.x + scaledWidgetPos.x + iconSize - (i * MoreHUDIconMargin * sizeFactor) - (i * iconRealSize));
 		currIcon.SetMember("_y", -iconPos.y + scaledWidgetPos.y - iconRealSize / 2.0);

 		currIcon.SetMember("_xscale", iconRealSize);
		currIcon.SetMember("_yscale", iconRealSize);
	}

	glm::dvec3 tmp;
	if (Get3DWidgetPos(GetUIFocusRef().get(), tmp))
		SetMoreHUDWidgetVisibility(true);
}

void SelectionWidget::SetMoreHUDWidgetVisibility(bool mode)
{
	std::lock_guard lock(m_manipulateMoreHUD);

	auto ui = RE::UI::GetSingleton();
	if (!ui) return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie) return;

	RE::GFxValue iconCont;
	hud->uiMovie->GetVariable(&iconCont, "ahz.scripts.widgets.AHZHudInfoWidget.IconContainer");
	if (!iconCont.IsObject()) return;

	if (mode)
		iconCont.Invoke("Show");
	else
		iconCont.Invoke("Hide");

	RE::GFxValue widgetCont;
	hud->uiMovie->GetVariable(&widgetCont, "AHZWidgetContainer.AHZWidget.content");
	if (!widgetCont.IsObject()) return;

	widgetCont.SetMember("_visible", mode);
}

void SelectionWidget::SetProgressCirlceValue(float newValue)
{
	if (Settings::IsDismountProgressCirlcleEnabled)
		ProgressCircleValue = newValue;
	else
		ProgressCircleValue = 0.0;
}

double SelectionWidget::GetWidgetTargetSize(FocusObject* focusObj)
{
    if (!focusObj || !focusObj->ObjectRef.get())
		return Settings::GetWidgetSizeMin();

	glm::dvec3 widgetPos;
	if (!Get3DWidgetPos(focusObj, widgetPos))
		return Widget2DSize;

	glm::vec3 glmWidgetPos(widgetPos.x, widgetPos.y, widgetPos.z);

	glm::vec3 cameraPos = Util::GetCameraPos();
	double distToObject = glm::length(cameraPos - glmWidgetPos);

	double distFactor = std::min(distToObject / GetMaxCameraDist(), 1.0);
	double distFactorInv = 1 - distFactor;

	double widgetSizeMin = Settings::GetWidgetSizeMin();
	double widgetSizeMax = Settings::GetWidgetSizeMax();

	return distFactorInv * (widgetSizeMax - widgetSizeMin) + widgetSizeMin;
}

glm::vec2 SelectionWidget::GetScreenLoc(const RE::GPtr<RE::IMenu>& menu, glm::vec3 worldPos)
{
	glm::vec3 screenLoc;
	RE::NiCamera::WorldPtToScreenPt3((float(*)[4])Offsets::WorldToCamMatrix.address(), *(RE::NiRect<float>*)Offsets::ViewPort.address(),
		RE::NiPoint3((float)worldPos.x, (float)worldPos.y, (float)worldPos.z),
		screenLoc.x, screenLoc.y, screenLoc.z, 1e-5f);

	return GetScreenLocFromPercentage(menu, screenLoc);
}

glm::vec2 SelectionWidget::GetScreenLocFromPercentage(RE::GPtr<RE::IMenu> menu, glm::vec2 posIn)
{
    if (!menu)
        return posIn;

	glm::vec2 posOut = posIn;
    RE::GRectF rect = SelectionWidgetMenu::GetMenuRect(menu.get());

	posOut.x = rect.left + (rect.right - rect.left) * posOut.x;
	posOut.y = 1.0f - posOut.y;  // flip y for Flash coordinate system
	posOut.y = rect.top + (rect.bottom - rect.top) * posOut.y;

	return posOut;
}

glm::vec2 SelectionWidget::ClampWidgetToScreenRect(const RE::GPtr<RE::IMenu>& menu, glm::vec2 posIn, RE::GFxValue selectionWidget)
{
	if (!selectionWidget.IsObject() || !menu)
		return posIn;

	auto activateText = GetGFxMember(selectionWidget, "ActivateText");
	if (!activateText.IsObject())
		return posIn;

	RE::GRectF rect = SelectionWidgetMenu::GetMenuRect(menu.get());
	double gfxTextWidth = GetGFxMember(activateText, "textWidth").GetNumber();
	double gfxWidgetHeight = GetGFxMember(selectionWidget, "_height").GetNumber();

	glm::vec2 halfTextBounds = PixelToStageCoordinates(menu, glm::vec2(gfxTextWidth, gfxWidgetHeight)) * 0.5f;

	if (posIn.x < halfTextBounds.x || posIn.x > rect.right - halfTextBounds.x ||
		posIn.y < halfTextBounds.y || posIn.y > rect.bottom - halfTextBounds.y)
	{
		glm::vec2 allowedMin(rect.left + halfTextBounds.x, rect.top + halfTextBounds.y);
		glm::vec2 allowedMax(rect.right - halfTextBounds.x, rect.bottom - halfTextBounds.y);
		// center location within allowed rect (accounting for text bounds)
		glm::vec2 allowedCenter = (allowedMin + allowedMax) * 0.5f;

		// direction from center to posIn
		glm::vec2 centerToPos = posIn - allowedCenter;
		glm::vec2 dirToPos = glm::normalize(centerToPos);

		float tX = (dirToPos.x > 0) 
            ? (allowedMax.x - allowedCenter.x) / dirToPos.x 
            : (allowedMin.x - allowedCenter.x) / dirToPos.x;
		float tY = (dirToPos.y > 0) 
			? (allowedMax.y - allowedCenter.y) / dirToPos.y 
			: (allowedMin.y - allowedCenter.y) / dirToPos.y;

		float moveMagnitude = std::min(tX, tY);

		// starting at allowedCenter, move on dirToPos for moveMagnitude screen units. This gives
		// a more accurate position (to the original 3D location) than a simple clamp
		posIn = allowedCenter + dirToPos * moveMagnitude;
	}

	return posIn;
}

bool SelectionWidget::Get3DWidgetPos(FocusObject* focusObj, glm::dvec3& posOut)
{
	// is ref still valid?
	if (!focusObj || !focusObj->ObjectRef.get() || focusObj->ObjectRef.get()->IsDisabled() || focusObj->ObjectRef.get()->IsMarkedForDeletion())
	{
		if (LastWidgetPos.has_value())
		{
			posOut = LastWidgetPos.value();
			return true;
		}

		return false;
	}

	auto focusRef = focusObj->ObjectRef.get();

	// place widget over characters' head, but in center for other objects, or for dead characters
	auto characterObject = focusRef->As<RE::Actor>();

	ObjectOverride objOverride;
	bool hasOverride = Settings::GetObjectOverride(focusRef.get(), objOverride);

	if (characterObject && (
		characterObject->AsActorState()->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kAlive ||
		characterObject->AsActorState()->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kBleedout ||
		characterObject->AsActorState()->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kDying ||
		characterObject->AsActorState()->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kEssentialDown ||
		characterObject->AsActorState()->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kRestrained ||
		characterObject->AsActorState()->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kReanimate
		))
	{
		auto raceName = characterObject->GetRace()->GetName();
		// draw name above bb for horses, instead of at bb center or head bone location
		if (strcmp(raceName, "Horse") == 0)
		{
			posOut = Util::GetBoundingBoxTop(focusObj);
		}
		else
		{
			auto characterHead = Util::GetCharacterHead(focusRef.get());
			if (characterHead)
			{
				auto worldTranslate = characterHead->world.translate;
				posOut = glm::dvec3(worldTranslate.x, worldTranslate.y, worldTranslate.z);
			}
			else
				posOut = Util::GetBoundingBoxCenter(focusObj);

			posOut.z += Settings::GetCurrentWidgetZOffset();
		}
	}
	else
	{
		if (hasOverride)
        {
            Get3DWidgetBasePos(focusObj, posOut, objOverride.WidgetBasePos);
			
			if (objOverride.HasWidgetPosOffset)
            {
                auto rotation = focusObj->CollisionObject.BoundingBox.rotation;
                posOut += objOverride.WidgetPosOffset;
            }
			else if (objOverride.HasWidgetPosOffsetRel)
			{
                auto rotation = focusObj->CollisionObject.BoundingBox.rotation;
                posOut += Util::RotateVector(rotation, objOverride.WidgetPosOffset);
			}
        }
		else
		{
            Get3DWidgetBasePos(focusObj, posOut, ObjectOverride::WidgetPos::Auto);
		}
	}
	
	return true;
}

bool SelectionWidget::Get3DWidgetBasePos(FocusObject* focusObj, glm::dvec3& posOut, ObjectOverride::WidgetPos pos)
{
	switch (pos)
	{
    case ObjectOverride::WidgetPos::Auto:

		return Get3DWidgetBasePosAuto(focusObj, posOut);

    case ObjectOverride::WidgetPos::Center:

		posOut = Util::GetBoundingBoxCenter(focusObj);
        return true;

    case ObjectOverride::WidgetPos::Top:

		posOut = Util::GetBoundingBoxTop(focusObj);
        return true;

    case ObjectOverride::WidgetPos::Bottom:

		posOut = Util::GetBoundingBoxBottom(focusObj);
        return true;

    case ObjectOverride::WidgetPos::Root:

		posOut = Util::GetObjectAccuratePosition(focusObj->ObjectRef.get().get());
        return true;
	}

	return false;
}

bool SelectionWidget::Get3DWidgetBasePosAuto(FocusObject* focusObj, glm::dvec3& posOut)
{
    auto focusRef = focusObj->ObjectRef.get().get();

    auto mesh = focusRef->GetCurrent3D();
    if (!mesh)
        return false;

    switch (auto collisionSize = FocusObject::GetCollisionSize(focusObj->CollisionObject.BoundingBox.boundMax.z))
    {
    case Tiny:
        posOut = Util::GetBoundingBoxTop(focusObj);

		posOut.z += Settings::GetCurrentWidgetZOffset();
        break;
    case Small:
        posOut = Util::GetBoundingBoxTop(focusObj);

		posOut.z += Settings::GetCurrentWidgetZOffset();
        break;
    default:
        posOut = Util::GetBoundingBoxCenter(focusObj);
        break;
    }

	return true;
}

glm::dvec2 SelectionWidget::Get3DWidgetSize(FocusObject* focusObj)
{
	double scaleFactor = GetWidgetTargetSize(focusObj);
	return glm::vec2(glm::vec2(OrigMCSize.x * scaleFactor, OrigMCSize.y * scaleFactor));
}

glm::dvec2 SelectionWidget::Get2DWidgetPos(const RE::GPtr<RE::IMenu>& menu)
{
	return GetScreenLocFromPercentage(menu, Widget2DPos);
}

glm::dvec2 SelectionWidget::Get2DWidgetSize()
{
	return glm::vec2(OrigMCSize.x * Widget2DSize, OrigMCSize.y * Widget2DSize);
}

glm::vec2 SelectionWidget::PixelToStageCoordinates(const RE::GPtr<RE::IMenu>& menu, glm::vec2 vecIn)
{
    RECT windowRect;
    GetClientRect(GetForegroundWindow(), &windowRect);

	RE::GRectF stageRect = menu->uiMovie->GetVisibleFrameRect();

	vecIn.x *= (stageRect.right / windowRect.right);
    vecIn.y *= (stageRect.bottom / windowRect.bottom);

	return vecIn;
}

glm::vec2 SelectionWidget::StageToPixelCoordinates(RE::GPtr<RE::IMenu> menu, glm::vec2 vecIn)
{
	if (!menu || !menu->uiMovie)
		return vecIn;

	RECT windowRect;
    GetClientRect(GetForegroundWindow(), &windowRect);

	RE::GRectF stageRect = menu->uiMovie->GetVisibleFrameRect();

	vecIn.x *= windowRect.right / stageRect.right;
    vecIn.y *= windowRect.bottom / stageRect.bottom;

	return vecIn;
}

glm::vec2 SelectionWidget::ToScaledAspectRatioCoordinates(RE::GPtr<RE::IMenu> menu, glm::vec2 vecIn)
{
	if (!menu || !menu->uiMovie)
		return vecIn;

    RECT windowRect;
    GetClientRect(GetForegroundWindow(), &windowRect);

	RE::GRectF menuStageRect = menu->uiMovie->GetSafeRect();

	float targetHeight = (float)windowRect.right * 0.5625f; // 16/9 = 0.5625f, Skyrim's Flash menus use 16/9 as standard
	float actualHeight = (float)windowRect.bottom;
	float heightPercentage = targetHeight / actualHeight;

	return glm::vec2( vecIn.x + ( (menuStageRect.right / 2.0) * (heightPercentage - 1.0) ),
		vecIn.y);
}

void SelectionWidget::SetElementPos(glm::vec2 screenPos, RE::GFxValue& element)
{
	element.SetMember("_x", screenPos.x);
	element.SetMember("_y", screenPos.y);
}

void SelectionWidget::SetElementSize(glm::vec2 size, RE::GFxValue& element)
{
 	element.SetMember("_xscale", size.x);
 	element.SetMember("_yscale", size.y);
}

void SelectionWidget::InitHUDValues()
{
	if (OriginalValuesSet)
		return;

	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie)
		return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie)
		return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");

	if (!selectionWidget.IsObject())
		return;

	OrigMCPos = glm::vec2(
		GetGFxMember(selectionWidget, "_x").GetNumber(),
		GetGFxMember(selectionWidget, "_x").GetNumber());

	OrigMCSize = glm::vec2(
		GetGFxMember(selectionWidget, "_xscale").GetNumber(),
		GetGFxMember(selectionWidget, "_yscale").GetNumber());

	OriginalValuesSet = true;

	Compatibility::MoreHUD::CheckModuleLoaded();
	Compatibility::SkyHUD::CheckModuleLoaded();

	logger::info("BTPS: successfully finished InitHUDValues");
}

void SelectionWidget::OnControlsToggled(bool mode)
{
	if (mode)
		SelectionWidgetMenu::Show("ControlsToggled");
	else
		SelectionWidgetMenu::Hide("ControlsToggled");
}

void SelectionWidget::OnFreeCamToggled(bool mode)
{
	if (!mode)
		SelectionWidgetMenu::Show("FreeCamToggled");
	else
		SelectionWidgetMenu::Hide("FreeCamToggled");
}

void SelectionWidget::FadeInInstant()
{
	SetUIFocusRef(FocusManager::GetActiveFocusObject());
	FadeMode = EUIFadeMode::NONE;
	FadeProgress = 0.0;
}

void SelectionWidget::FadeOutInstant()
{
	EndFadeOut();
}

void SelectionWidget::StartFadeIn()
{
	// don't update focus object now, but schedule for update as soon as activation text changes
	UpdateFocusObjDeferred = true;

	// invert previous fade progress
	if (FadeProgress > 0.01)
		FadeProgress = 1.0 - FadeProgress;
	else
		FadeProgress = 0.0;

	FadeMode = EUIFadeMode::FADE_IN;
}

void SelectionWidget::StartFadeOut()
{
	// invert previous fade progress
	if (FadeProgress > 0.01)
		FadeProgress = 1.0 - FadeProgress;
	else
		FadeProgress = 0.0;

	FadeMode = EUIFadeMode::FADE_OUT;
}

void SelectionWidget::EndFadeIn()
{
	FadeMode = NONE;
}

void SelectionWidget::EndFadeOut()
{
	SetUIFocusRef(FocusManager::GetActiveFocusObject());
	FadeMode = FADED;

	LastWidgetPos.reset();

	ClearSelectionText();
}

void SelectionWidget::ClearSelectionText()
{
}

void SelectionWidget::ProgressFade()
{
	switch (FadeMode)
	{
		case FADE_IN:
			FadeProgress += Settings::WidgetFadeInDelta; break;
		case FADE_OUT:
			FadeProgress += Settings::WidgetFadeOutDelta; break;
		case FADED:
		case NONE:
			break;
	}

	if (FadeProgress > 1.0)
	{
		FadeProgress = 1.0;

		switch (FadeMode)
		{
			case FADE_IN:
				EndFadeIn(); break;
			case FADE_OUT:
				EndFadeOut(); break;
			case FADED:
			case NONE:
				break;
		}
	}
}

SelectionWidgetMenu::SelectionWidgetMenu()
{
	auto scaleformManager = RE::BSScaleformManager::GetSingleton();
	if (!scaleformManager)
	{
		logger::error("BTPS: failed to initialize SelectionWidgetMenu");
		return;
	}

	depthPriority = 0;

	menuFlags.set(RE::UI_MENU_FLAGS::kAlwaysOpen);
	menuFlags.set(RE::UI_MENU_FLAGS::kRequiresUpdate);
	menuFlags.set(RE::UI_MENU_FLAGS::kAllowSaving);

	inputContext = Context::kNone;

	if (uiMovie)
	{
		uiMovie->SetMouseCursorCount(0); // disable input
		uiMovie->SetVisible(false);
	}

	scaleformManager->LoadMovieEx(this, MENU_PATH, [this](RE::GFxMovieDef* a_def) -> void
	{
		a_def->SetState(RE::GFxState::StateType::kLog,
			RE::make_gptr<Logger>().get());

		logger::info("BTPS: SelectionWidgetMenu loaded flash");
	});

	InitProgressCirclePosition(this);
}

void SelectionWidgetMenu::InitProgressCirclePosition(SelectionWidgetMenu* menu)
{
	if (!menu || !menu->uiMovie)
		return;

	RE::GFxValue progressCirlce;
	menu->uiMovie->GetVariable(&progressCirlce, "ProgressCircleInstance");

	if (!progressCirlce.IsObject())
		return;

	auto menuRect = GetMenuRect(menu);

	progressCirlce.SetMember("_x", menuRect.right / 2.0f);
    progressCirlce.SetMember("_y", menuRect.bottom / 2.0f);
}

void SelectionWidgetMenu::Register()
{
	auto ui = RE::UI::GetSingleton();
	if (ui)
	{
		ui->Register(MENU_NAME, Creator);

		logger::info("BTPS: successfully registered SelectionWidgetMenu");
	}
	else
		logger::error("BTPS: failed to register SelectionWidgetMenu");
}

RE::GRectF SelectionWidgetMenu::GetMenuRect(const RE::IMenu* menu)
{
    if (!menu || !menu->uiMovie)
        return {};

    return menu->uiMovie->GetVisibleFrameRect();
}

void SelectionWidgetMenu::Load()
{
	std::lock_guard lock(m_load);

    if (auto msgQ = RE::UIMessageQueue::GetSingleton())
		msgQ->AddMessage(MENU_NAME, RE::UI_MESSAGE_TYPE::kShow, nullptr);
	else
		logger::warn("BTPS: failed to load SelectionWidgetMenu");
}

void SelectionWidgetMenu::Unload()
{
	std::lock_guard lock(m_unload);

    if (auto msgQ = RE::UIMessageQueue::GetSingleton())
		msgQ->AddMessage(MENU_NAME, RE::UI_MESSAGE_TYPE::kHide, nullptr);
	else
		logger::warn("BTPS: failed to unload SelectionWidgetMenu");
}

double SelectionWidget::GetWidgetCurrentAlpha()
{
	double alphaBase = Settings::WidgetAlpha;

	switch (SelectionWidget::FadeMode)
	{
		case FADE_IN:
			return alphaBase * SelectionWidget::FadeProgress;
		case FADE_OUT:
			return alphaBase * (1.0 - SelectionWidget::FadeProgress);
		case FADED:
			return 0.0;
		case NONE:
			return alphaBase;
	}
	return alphaBase;
}

void SelectionWidget::SetSelectionAlpha(double alpha, RE::GFxValue& selectionWidget)
{
	auto activateText = GetGFxMember(selectionWidget, "ActivateText");
	if (activateText.IsObject())
		activateText.SetMember("_alpha", alpha);

	auto infoText = GetGFxMember(selectionWidget, "InfoText");
	if (infoText.IsObject())
		infoText.SetMember("_alpha", alpha);

	auto grayBar = GetGFxMember(selectionWidget, "GrayBar");
	if (grayBar.IsObject())
		grayBar.SetMember("_alpha", alpha);

	auto activateButton = GetGFxMember(selectionWidget, "ActivateButtonInstance");
	if (!activateButton.IsObject())
		return;

	auto activateButtonText = GetGFxMember(activateButton, "Text");
	if (!activateButtonText.IsObject())
		return;

	activateButtonText.SetMember("_alpha", alpha);
}

void SelectionWidgetMenu::Show(const std::string& source)
{
	std::lock_guard lock(m_show);

	if (Hidden_Sources.empty())
		return;

	if (!source.empty())
	{
		auto sourceIdx = std::find(Hidden_Sources.begin(), Hidden_Sources.end(), source);
		if (sourceIdx != Hidden_Sources.end())
			Hidden_Sources.erase(sourceIdx);
	}

	if (IsEnabled && Hidden_Sources.empty())
	{
		ToggleVisibility(true);
		SelectionWidget::Update();
	}
}

void SelectionWidgetMenu::Hide(const std::string& source)
{
	std::lock_guard lock(m_hide);

	auto sourceIdx = std::find(Hidden_Sources.begin(), Hidden_Sources.end(), source);
	if (sourceIdx == Hidden_Sources.end())
	{
		Hidden_Sources.push_back(source);

		ToggleVisibility(false);
		SelectionWidget::Update();
	}
}

void SelectionWidgetMenu::ToggleVisibility(const bool mode)
{
	std::lock_guard lock(m_toggleVisibility);

	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie)
		return;

	widgetMenu->uiMovie->SetVisible(mode);
	SelectionWidget::HideNativeHUD();

	IsVisible = mode;
}

void SelectionWidgetMenu::AdvanceMovie(float a_interval, std::uint32_t a_currentTime)
{
	RE::IMenu::AdvanceMovie(a_interval, a_currentTime);

	SelectionWidget::HideNativeHUD();
}
