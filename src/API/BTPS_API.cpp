#include "BTPS_API.h"

#include "FocusManager.h"
#include "Settings.h"
#include "UI/SelectionWidget.h"

std::mutex m_singleton;
std::mutex m_selectionEnabled;
std::mutex m_widget3DEnabled;
std::mutex m_showWidget;
std::mutex m_hideWidget;

BTPS_API* BTPS_API::GetSingleton()
{
	std::lock_guard<std::mutex> lock(m_singleton);

	static BTPS_API singleton;
	return std::addressof(singleton);
}

bool BTPS_API::SelectionEnabled()
{
	std::lock_guard<std::mutex> lock(m_selectionEnabled);
	return FocusManager::GetIsEnabled();
}

bool BTPS_API::Widget3DEnabled()
{
	std::lock_guard<std::mutex> lock(m_widget3DEnabled);
	return Settings::GetWidget3DEnabled();
}


void BTPS_API::HideSelectionWidget(std::string source)
{
	std::lock_guard<std::mutex> lock(m_hideWidget);
	SelectionWidgetMenu::Hide(source);
}

void BTPS_API::ShowSelectionWidget(std::string source)
{
	std::lock_guard<std::mutex> lock(m_showWidget);
	SelectionWidgetMenu::Show(source);
}
